﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Spider_Problem
{
    class Program
    {
        static void Main(string[] args)
        {
            SpiderQueue queue = GetSpiderQueue();
            string answer = queue.GetSelectedPositions();
            Console.WriteLine(answer);
            //Console.ReadLine();        // For testing: Prevent console window from disappearing.
        }

        static SpiderQueue GetSpiderQueue()
        {
            Input input = new Input();
            return new SpiderQueue(input);
        }
    }

    public class Input
    {
        public string Config { get; set; }
        public string Powers { get; set; }

        public Input()
        {
            Config = Console.ReadLine();
            Powers = Console.ReadLine();
        }
    }

    public class SpiderQueue
    {
        public QueueConfig Config { get; set; }
        public Queue<Spider> Queue { get; set; }

        public SpiderQueue(Input input)
        {
            Config = new QueueConfig(input.Config);
            Queue = ParsePowersInput(input.Powers);
        }

        Queue<Spider> ParsePowersInput(string powers)
        {
            Queue<Spider> queue = new Queue<Spider>();
            string[] powerArray = powers.Split(' ');
            for (int position = 0; position < powerArray.Length; position++)
            {
                int.TryParse(powerArray[position], out int power);
                queue.Enqueue(new Spider(power, position));
            }
            return queue;
        }

        public string GetSelectedPositions()
        {
            PositionList<int> selectedPositions = new PositionList<int>();
            for (int iteration = 0; iteration < Config.Iterations; iteration++)
            {
                selectedPositions.Add(GetNextSelection());
            }
            return selectedPositions.ToString();
        }

        int GetNextSelection()
        {
            SpiderBatch batch = DequeueNextBatch();
            Spider selectedSpider = batch.SelectMostPowerful();
            List<Spider> batchRemainder = new SpiderBatch(batch.Remove(selectedSpider)).DecrementPowers();
            ReEnqueue(batchRemainder);
            return selectedSpider.InitialPosition;
        }

        SpiderBatch DequeueNextBatch()
        {
            List<Spider> batch = new List<Spider>();
            while (CanDequeue() && CanAddToBatch(batch))
            {
                batch.Add(Queue.Dequeue());
            }
            return new SpiderBatch(batch);
        }

        bool CanDequeue()
        {
            return Queue.Count > 0;
        }

        bool CanAddToBatch(List<Spider> batch)
        {
            return batch.Count < Config.BatchSize;
        }

        void ReEnqueue(List<Spider> spiders)
        {
            foreach (var spider in spiders)
            {
                Queue.Enqueue(spider);
            }
        }
    }

    public class QueueConfig
    {
        public string ConfigInput { get; set; }
        public int TotalSize { get; set; }
        public int BatchSize { get; set; }
        public int Iterations { get; set; }

        public QueueConfig(string configInput)
        {
            ConfigInput = configInput;
            TotalSize = ParseConfigParam(0);
            BatchSize = ParseConfigParam(1);
            Iterations = BatchSize;
        }

        int ParseConfigParam(int paramIndex)
        {
            string paramValue = ConfigInput.Split(' ')[paramIndex];
            int.TryParse(paramValue, out int value);
            return value;
        }
    }

    public class Spider
    {
        public int Power { get; set; }
        public int InitialPosition { get; set; }

        public Spider(int power, int zeroBaseIndex)
        {
            Power = power;
            InitialPosition = zeroBaseIndex + 1;
        }

        public void DecrementPower()
        {
            if (Power > 0)
            {
                Power--;
            }
        }
    }

    public class SpiderBatch
    {
        public List<Spider> Spiders { get; set; }

        public SpiderBatch(List<Spider> spiders)
        {
            Spiders = spiders;
        }

        public Spider SelectMostPowerful()
        {
            Spider selected = Spiders[0];
            foreach (var spider in Spiders)
            {
                selected = ComparePowers(spider, selected);
            }
            return selected;
        }

        Spider ComparePowers(Spider currentSpider, Spider selectedSpider)
        {
            if (currentSpider.Power > selectedSpider.Power)
            {
                return currentSpider;
            }
            return selectedSpider;
        }

        public List<Spider> Remove(Spider selectedSpider)
        {
            Spiders.Remove(selectedSpider);
            return Spiders;
        }

        public List<Spider> DecrementPowers()
        {
            foreach (var spider in Spiders)
            {
                spider.DecrementPower();
            }
            return Spiders;
        }
    }

    public class PositionList<T> : List<T>
    {
        public override string ToString()
        {
            StringBuilder builder = new StringBuilder("");
            int lastIndexInList = this.Count - 1;
            for (int listIndex = 0; listIndex < lastIndexInList; listIndex++)
            {
                builder.Append(this[listIndex].ToString() + " ");
            }
            builder.Append(this[lastIndexInList]);
            return builder.ToString();
        }
    }
}
